#---Creates a pdf file with the charts

pdf("visualization.pdf")

library(tidyverse)  #load tidyverse
library(readr) #load readr


long_data_ <- read_csv("long_data_.csv") #import long_data_.csv file
state_wise_population_2019 <- read_csv("state_wise_population__2019.csv") ##import long_data_.csv file

statesusage <-long_data_[, c("States", "Usage")] #crete df has only the specified columns from long_data_

repdata <- statesusage %>%  select(States, Usage) %>% filter(States == "Gujarat" | States == "Maharashtra" | States == "Rajasthan" | States == "Tamil Nadu" | States == "Telangana" | States == "Andhra Pradesh" | States == "Karnataka" | States == "West Bengal" | States == "UP" | States == "Uttarakhand") #crete df has only the specified rownames from long_data_
SAU <- aggregate(x = statesusage$Usage, by = list(statesusage$States), FUN = sum)

colnames(SAU)[colnames(SAU) %in% c("Group.1", "x")] <- c("STATES", "USAGE") # changing the file name

dataset <- state_wise_population_2019[ ,c("State", "total_population")] #create df has only the specified columns from long_data_


usage <- dataset[c(2,10,15,18,26,28,29,31,32,33), ] ##specific rows are selected to a df



ten_states <- SAU[c(1, 10, 15, 17, 26, 28, 29, 31, 32, 33), ] #specific rows are selected to a df

TEN_STATES <- cbind(ten_states, usage) #columns is binded

TEN_STATES$mean_usage_percapita <- TEN_STATES$USAGE*1000/TEN_STATES$total_population #per capita power consumption formula

boxplot(repdata$Usage ~ repdata$States, repdata, xlab = "DIFFERENT STATES IN INDIA", ylab = "POWER CONSUMPTION KW.h", main = "POWER CONSUMPTION PER CAPITA", col="yellow");points(1:10, TEN_STATES$mean_usage_percaptia, col = "red", pch = 10, type = "o") #boxplot

meanusage <- mean(repdata$Usage) #calculte a mean

standardusage <- sd(repdata$Usage) #calculate sd

histogram <- hist(repdata$Usage,10, main = "FREQUENCY OF POWER CONSUMPTION", xlab = "USAGE", ylab = "FREQUENCY", border = "blue", col = "cyan", density = 30, xlim = c(0,600), breaks = 10)  #Histogram is saves to a variable

hist(repdata$Usage,10, main = "FREQUENCY OF POWER CONSUMPTION", xlab = "USAGE", ylab = "FREQUENCY", border = "blue", col = "cyan", density = 30, xlim = c(0,600), breaks = 10) #Histogram graph



xp <- seq(0,600,1) # x-axis values from 0 to 100 with intervals

a1 <- dnorm(xp, mean=meanusage, sd=standardusage) #normal curve having frequency 

a1 <- a1 * diff(histogram$mids[1:2]) * length(repdata$Usage); 

lines(xp, a1, col = "red") #creates the line cure with the values to it


#analyis_start
usage <- dataset[c(3,5,6,1,10,7,32,2,20,4), ] ##specific rows are selected to a df
TEN_STATES <- cbind(ten_states, usage) #columns is binded
#calulating the mean and the std.dev
new_data<- TEN_STATES

Entries<-503


# cal mean per captia
new_data$mean_usage_per_captia<-(new_data$USAGE/new_data$total_population)*1000

# cal std_dev_per captia

new_data$std_dev_per_captia<-NA

new_data$std_dev_per_captia[1]<-sd(repdata$Usage[repdata$States=="Andhra Pradesh"]*503/TEN_STATES$total_population[TEN_STATES$STATES=="Andhra Pradesh"])  
new_data$std_dev_per_captia[2]<-sd(repdata$Usage[repdata$States=="Gujarat"]*503/TEN_STATES$total_population[TEN_STATES$STATES=="Gujarat"])  
new_data$std_dev_per_captia[3]<-sd(repdata$Usage[repdata$States=="Karnataka"]*503/TEN_STATES$total_population[TEN_STATES$STATES=="Karnataka"])  
new_data$std_dev_per_captia[4]<-sd(repdata$Usage[repdata$States=="Maharashtra"]*503/TEN_STATES$total_population[TEN_STATES$STATES=="Maharashtra"])  
new_data$std_dev_per_captia[5]<-sd(repdata$Usage[repdata$States=="Rajasthan"]*503/TEN_STATES$total_population[TEN_STATES$STATES=="Rajasthan"])  
new_data$std_dev_per_captia[6]<-sd(repdata$Usage[repdata$States=="Tamil Nadu"]*503/TEN_STATES$total_population[TEN_STATES$STATES=="Tamil Nadu"])  
new_data$std_dev_per_captia[7]<-sd(repdata$Usage[repdata$States=="Telangana"]*503/TEN_STATES$total_population[TEN_STATES$STATES=="Telangana"])  
new_data$std_dev_per_captia[8]<-sd(repdata$Usage[repdata$States=="UP"]*503/TEN_STATES$total_population[TEN_STATES$STATES=="UP"])  
new_data$std_dev_per_captia[9]<-sd(repdata$Usage[repdata$States=="Uttarakhand"]*Entries/TEN_STATES$total_population[TEN_STATES$STATES=="Uttarakhand"])
new_data$std_dev_per_captia[10]<-sd(repdata$Usage[repdata$States=="West Bengal"]*Entries/TEN_STATES$total_population[TEN_STATES$STATES=="West Bengal"])




#analyis coding


var.test(((repdata$Usage[repdata$States == "Gujarat"]*Entries)/TEN_STATES$total_population[TEN_STATES$STATES=="Gujarat"])*1000, ((repdata$Usage[repdata$States == "Andhra Pradesh"]*Entries)/  TEN_STATES$total_population[TEN_STATES$STATES=="Andhra Pradesh"])*1000,)
t.test((repdata$Usage[repdata$States == "Gujarat"]*Entries/TEN_STATES$total_population[TEN_STATES$STATES=="Gujarat"])*1000, 
       (repdata$Usage[repdata$States == "Andhra Pradesh"]*Entries/TEN_STATES$total_population[TEN_STATES$STATES=="Andhra Pradesh"])*1000,
       data = repdata,var.equal = TRUE)


var.test(((repdata$Usage[repdata$States == "Karnataka"]*Entries)/TEN_STATES$total_population[TEN_STATES$STATES=="Karnataka"])*1000, ((repdata$Usage[repdata$States == "Maharashtra"]*Entries)/  TEN_STATES$total_population[TEN_STATES$STATES=="Maharashtra"])*1000)
t.test((repdata$Usage[repdata$States == "Karnataka"]*Entries/TEN_STATES$total_population[TEN_STATES$STATES=="Karnataka"])*1000, 
       (repdata$Usage[repdata$States == "Maharashtra"]*Entries/TEN_STATES$total_population[TEN_STATES$STATES=="Maharashtra"])*1000,
       data = repdata,var.equal = TRUE)



var.test(((repdata$Usage[repdata$States == "UP"]*Entries)/TEN_STATES$total_population[TEN_STATES$STATES=="UP"])*1000, ((repdata$Usage[repdata$States == "Telangana"]*Entries)/  TEN_STATES$total_population[TEN_STATES$STATES=="Telangana"])*1000)
t.test((repdata$Usage[repdata$States == "UP"]*Entries/TEN_STATES$total_population[TEN_STATES$STATES=="UP"])*1000, 
       (repdata$Usage[repdata$States == "Telangana"]*Entries/TEN_STATES$total_population[TEN_STATES$STATES=="Telangana"])*1000,
       data = repdata,var.equal = TRUE)




var.test(((repdata$Usage[repdata$States == "Uttarakhand"]*Entries)/TEN_STATES$total_population[TEN_STATES$STATES=="Uttarakhand"])*1000, ((repdata$Usage[repdata$States == "West Bengal"]*Entries)/  TEN_STATES$total_population[TEN_STATES$STATES=="West Bengal"])*1000)
t.test((repdata$Usage[repdata$States == "Uttarakhand"]*Entries/TEN_STATES$total_population[TEN_STATES$STATES=="Uttarakhand"])*1000, 
       (repdata$Usage[repdata$States == "West Bengal"]*Entries/TEN_STATES$total_population[TEN_STATES$STATES=="West Bengal"])*1000,
       data = repdata,var.equal = TRUE)



var.test(((repdata$Usage[repdata$States == "Karnataka"]*Entries)/TEN_STATES$total_population[TEN_STATES$STATES=="Karnataka"])*1000, ((repdata$Usage[repdata$States == "Andhra Pradesh"]*Entries)/  TEN_STATES$total_population[TEN_STATES$STATES=="Andhra Pradesh"])*1000)
t.test((repdata$Usage[repdata$States == "Karnataka"]*Entries/TEN_STATES$total_population[TEN_STATES$STATES=="Karnataka"])*1000, 
       (repdata$Usage[repdata$States == "Andhra Pradesh"]*Entries/TEN_STATES$total_population[TEN_STATES$STATES=="Andhra Pradesh"])*1000,
       data = repdata,var.equal = TRUE)


var.test(((repdata$Usage[repdata$States == "Telangana"]*Entries)/TEN_STATES$total_population[TEN_STATES$STATES=="Telangana"])*1000, ((repdata$Usage[repdata$States == "West Bengal"]*Entries)/  TEN_STATES$total_population[TEN_STATES$STATES=="West Bengal"])*1000)
t.test((repdata$Usage[repdata$States == "Telangana"]*Entries/TEN_STATES$total_population[TEN_STATES$STATES=="Telangana"])*1000, 
       (repdata$Usage[repdata$States == "West Bengal"]*Entries/TEN_STATES$total_population[TEN_STATES$STATES=="West Bengal"])*1000,
       data = repdata,var.equal = TRUE)


var.test(((repdata$Usage[repdata$States == "Andhra Pradesh"]*Entries)/TEN_STATES$total_population[TEN_STATES$STATES=="Andhra Pradesh"])*1000, ((repdata$Usage[repdata$States == "West Bengal"]*Entries)/  TEN_STATES$total_population[TEN_STATES$STATES=="West Bengal"])*1000)
t.test((repdata$Usage[repdata$States == "Andhra Pradesh"]*Entries/TEN_STATES$total_population[TEN_STATES$STATES=="Andhra Pradesh"])*1000, 
       (repdata$Usage[repdata$States == "West Bengal"]*Entries/TEN_STATES$total_population[TEN_STATES$STATES=="West Bengal"])*1000,
       data = repdata,var.equal = TRUE)


#PAIRED T-TEST
t.test((repdata$Usage[repdata$States == "Gujarat"]*Entries/TEN_STATES$total_population[TEN_STATES$STATES=="Gujarat"])*1000, 
       (repdata$Usage[repdata$States == "Andhra Pradesh"]*Entries/TEN_STATES$total_population[TEN_STATES$STATES=="Andhra Pradesh"])*1000,
       data = repdata,var.equal = TRUE,paired=TRUE)

t.test((repdata$Usage[repdata$States == "Karnataka"]*Entries/TEN_STATES$total_population[TEN_STATES$STATES=="Karnataka"])*1000, 
       (repdata$Usage[repdata$States == "Maharashtra"]*Entries/TEN_STATES$total_population[TEN_STATES$STATES=="Maharashtra"])*1000,
       data = repdata,var.equal = TRUE,paired=TRUE)

t.test((repdata$Usage[repdata$States == "Tamil Nadu"]*Entries/TEN_STATES$total_population[TEN_STATES$STATES=="Tamil Nadu"])*1000, 
       (repdata$Usage[repdata$States == "Rajasthan"]*Entries/TEN_STATES$total_population[TEN_STATES$STATES=="Rajasthan"])*1000,
       data = repdata,var.equal = TRUE,paired=TRUE)

t.test((repdata$Usage[repdata$States == "Uttarakhand"]*Entries/TEN_STATES$total_population[TEN_STATES$STATES=="Uttarakhand"])*1000, 
       (repdata$Usage[repdata$States == "West Bengal"]*Entries/TEN_STATES$total_population[TEN_STATES$STATES=="West Bengal"])*1000,
       data = repdata,var.equal = TRUE,paired=TRUE)

t.test((repdata$Usage[repdata$States == "Karnataka"]*Entries/TEN_STATES$total_population[TEN_STATES$STATES=="Karnataka"])*1000, 
       (repdata$Usage[repdata$States == "Andhra Pradesh"]*Entries/TEN_STATES$total_population[TEN_STATES$STATES=="Andhra Pradesh"])*1000,
       data = repdata,var.equal = TRUE,paired=TRUE)


t.test((repdata$Usage[repdata$States == "UP"]*Entries/TEN_STATES$total_population[TEN_STATES$STATES=="UP"])*1000, (repdata$Usage[repdata$States == "Telegana"]*Entries/TEN_STATES$total_population[TEN_STATES$STATES=="Telegana"])*1000,data = repdata,var.equal = TRUE,paired=TRUE)

dev.off()





